alert ('Hello,Frontenders!') 

// A task №1 
let a = '100px';
let b = '323px';
let result = parseInt(a)+parseInt(b);
console.log(result);
// A task №2
console.log(Math.max(10, -45, 102, 36, 12, 0, -1));
// A task №3 
let c =123.3399;
console.log(Math. round(c))

let d =0.111;
console.log(Math. ceil(d))

let e =45.333333;
console.log(e.toFixed(1))

let f =3;
console.log(f**5)

let g =400000000000000;
console.log(4e14)

let h ="1"!=1;
console.log("1" == 1);

console.log(0.1+0.2===0.3);// Вернёт false, почему?
// Число хранится в памяти в бинарной форме, 
// как последовательность бит – единиц и нулей. 
// Но дроби, такие как 0.1, 0.2,
// которые выглядят довольно просто в десятичной системе счисления,
// на самом деле являются бесконечной дробью в двоичной форме.
// когда мы суммируем 2 числа, их «неточности» тоже суммируются.
// Вот почему 0.1 + 0.2 – это не совсем 0.3.

