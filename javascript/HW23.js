 //exercise 1
let count = +prompt ('Введите число');
let intervalId = setInterval(() => {
    count = count - 1;
    console.log("осталось", count);
    if (count === 0) {
        clearInterval(intervalId);
        console.log('Время вышло...');
    }
    if (isNaN(count)) {
        alert ('Ошибка. Вы ввели не число');
        clearInterval(intervalId);
    }
}, 1000);   

//exercise 2

let promise = fetch("https://reqres.in/api/users");
promise
.then(function(response) {
    return response.json();
})
.then(function (response) {
    let users = response.data;
    console.log(`получили пользователей: ${users.length}`)
    users.forEach(function(user) {
        console.log(`-${user.first_name}  ${user.last_name} (${user.email})`)
    })
})
