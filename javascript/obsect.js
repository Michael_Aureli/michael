"use strict";
//HW
//exercise 1
let user = {
  name: "Michael",
  weight: 70,
};

//Проверяет объект на наличие свойств
//@param {object} obj объект, проверяемый на наличие свойств
// @return {boolean} true, если у объекта нет свойств, иначе false

function isEmpty(user) {
  for (let key in user) {
    return false;
  }
  return true;
}
console.log(isEmpty(user));

//exercise 3

//позволит произвести повышение зарплаты на определенный процент
//@param {number} raiseSalary(perzent)  процент
//@return {object} salaries объекты с повышенными зарплатами

let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000,
};
function raiseSalary(perzent) {
  for (let key in salaries) {
        let raise = (salaries[key] * perzent) / 100;
    salaries[key] = +salaries[key].toFixed(0);
  }
  return salaries;
}

// Сумма значения всех зарплат
//@param {object} salaries 
//@return {number} sum cумма зарплат сотрудников.

function summarizesSalary(salaries) {
  let sum = 0;      
  for (let key in salaries) {
    sum += salaries[key];
  }
  return sum;
}
console.log(summarizesSalary(raiseSalary(5)));
