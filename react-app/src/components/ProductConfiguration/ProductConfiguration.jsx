import { useState } from 'react';
import './ProductConfiguration.css';
import ConfigurationButton from './ConfigurationButton';



function ProductConfiguration(props) {
    let { buttons } = props
    const [idActive, setIdActive] = useState(JSON.parse(localStorage.getItem('configChoise')));
    const handleClick = (id) => setIdActive(id);
    localStorage.setItem('configChoise', JSON.stringify(idActive));

    return (
        <div>
            <section className="product-configuration">
                <div className="product-configuration__name">
                    <h3 className="h3">Конфигурация памяти: 128 ГБ</h3>
                </div>
                <div className="product-configuration__buttons">
                    {
                        buttons.map((item) =>

                            <div onClick={() => handleClick(item.id)} key={item.id}>
                                <ConfigurationButton 
                                    item={item}
                                    isActive={item.id === idActive} 
                                    />
                            </div>
                        )
                    }
                </div>
            </section>
        </div>
    );
}

export default ProductConfiguration;





