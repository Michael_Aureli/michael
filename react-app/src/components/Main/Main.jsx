import './Main.css';
import ProductViews from '../ProductViews/ProductViews';
import ProductColor from '../ProductColor/ProductColor';
import ProductConfiguration from '../ProductConfiguration/ProductConfiguration';
import ProductCharacteristics from '../ProductCharacteristics/ProductCharacteristics';
import ProductDescription from '../ProductDescription/ProductDescription';
import ProductComparison from '../ProductComparison/ProductComparison';
import Reviews from '../Reviews/Reviews';
import Form from '../Form/Form';
import Sidebar from '../Sidebar/Sidebar';
import MessageCart from '../MessageCart/MessageCart';

import ColorPhoto1 from './color-12.png';
import ColorPhoto2 from './color-21.png';
import ColorPhoto3 from './color-31.png';
import ColorPhoto4 from './color-41.png';
import ColorPhoto5 from './color-51.png';
import ColorPhoto6 from './color-61.png';
import MarkG from './Mark.svg';
import Valery from './Valery.svg';


import { useState } from 'react';


let characteristicitem = [
    {
        text: 'Apple A15 Bionic',
        link: 'https://ru.wikipedia.org/wiki/Apple_A15',
    },
];

let colorButtons = [
    {
        src: (ColorPhoto1),
        className: "product-color__img",
        id: 1,
        color: 'Red',
        name: 'красный'
    },
    {
        src: (ColorPhoto2),
        className: "product-color__img",
        id: 2,
        color: 'Green',
        name: 'зеленый'
    },
    {
        src: (ColorPhoto3),
        className: "product-color__img",
        id: 3,
        color: 'Pink',
        name: 'розовый'

    },
    {
        src: (ColorPhoto4),
        className: "product-color__img",
        id: 4,
        color: 'Blue',
        name: 'синий'
    },
    {
        src: (ColorPhoto5),
        className: "product-color__img",
        id: 5,
        color: 'White',
        name: 'белый'
    },
    {
        src: (ColorPhoto6),
        className: "product-color__img",
        id: 6,
        color: 'Black',
        name: 'черный'
    },
];

let configurationButtons = [
    {
        text: '128 ГБ',
        className: "product-configuration__button",
        id: 1
    },
    {
        text: '256 ГБ',
        className: "product-configuration__button",
        id: 2
    },
    {
        text: '512 ГБ',
        className: "product-configuration__button",
        id: 3
    },
];


let reviewItems = [
    {
        name: 'Марк Г.',
        src: (MarkG),
        experience: 'менее месяца',
        pluses: `это мой первый айфон после огромного количества телефонов на андроиде. всё
        плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,
        ширик тоже на высоте.`,
        minuses: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская
        вещь) а если нужно переносить фото с компа, то это только через itunes, который
        урезает качество фотографий исходное`
    },
    {
        name: 'Валерий Коваленко',
        src: (Valery),
        experience: 'менее месяца',
        pluses: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
        minuses: `Плохая ремонтопригодность`
    },
];

let rating = [
    ['star1', 'star1', 'star1', 'star1', 'star1'],
    ['star2', 'star1', 'star1', 'star1', 'star1']
];

function Main(props) {

    const { handleClickButton, handleClickLike, ActiveButton, ActiveLike} = props
    const [colorActive, setColorActive] = useState(localStorage.getItem('colorChoise') || 'Blue');
    const handleClickColor = (color) => setColorActive(color);
    localStorage.setItem('colorChoise', colorActive);

    return (
        <div>

            {/* <ProductViews /> */}

            <ProductViews buttons={colorButtons} colorActive={colorActive} />

            <div className="product-container">
                <div className="product-information">

                    <ProductColor buttons={colorButtons} />
                    <ProductConfiguration buttons={configurationButtons} />
                    <ProductCharacteristics item={characteristicitem} />
                    <ProductDescription />
                    <ProductComparison />

                    <section className="reviews-container">

                        <Reviews item={reviewItems} rating={rating} />

                        <Form />

                    </section>
                </div>

                <Sidebar
                    handleClickButton={handleClickButton}
                    ActiveButton={ActiveButton}
                    handleClickLike={handleClickLike}
                    ActiveLike={ActiveLike}
                />
                <MessageCart
                 ActiveButton={ActiveButton}
                 />

            </div>
        </div>
    );
}

export default Main;