import './Iframe.css';
import styled from "styled-components";

const IframeContent = styled.div`
padding: 0px;
margin: 0px;
height: 300px;
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
width: 100%;
background: #F0F0F0;
`;


function Iframe() {
  return (
    <IframeContent>
       Здесь могла бы быть ваша реклама
    </IframeContent>
        );
}

export default Iframe;