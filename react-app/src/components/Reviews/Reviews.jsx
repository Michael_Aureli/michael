import './Reviews.css';
import Review from './Review';


function Reviews(props) {

    let { item, rating} = props
    
    return (
        <div>
            <div className="reviews-header">
                <div className="reviews-header__text">
                    Отзывы
                    <span className="reviews-header__number">425</span>
                </div>
            </div>

            <div className="reviews-body">

                {
                    item.map(function ({ name, src, experience, pluses, minuses }, index) {
                      
                        if (index) {
                            return (

                                <Review name={name} src={src} rating={rating[1]} experience={experience} pluses={pluses} minuses={minuses}  key = {index}/>

                            )
                            
                        } else {
                            return (
                                <div key={index}>
                                    <Review name={name} src={src} rating={rating[0]} experience={experience} pluses={pluses} minuses={minuses} />
                                    <div><hr className="reviews-body__separator" /></div>
                                </div>
                            )
                        }
                        
                    })
                }

            </div>
        </div>
    );
}

export default Reviews;
