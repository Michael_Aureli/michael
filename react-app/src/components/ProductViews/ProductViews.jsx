import styles from './ProductViews.module.css';
import imageView1 from './image-1.webp';
import imageView2 from './image-2.webp';
import imageView3 from './image-3.webp';
import imageView4 from './image-4.webp';
import imageView5 from './image-5.webp';





function ProductViews() {
    return (
        <div>
            <h2 className={styles.h2}>
                Смартфон Apple iPhone 13 синий
            </h2>
            <section className={styles.imagesViews}>
                <img className={styles.imagesViewsPhoto} src={imageView1} alt="View of product" />
                <img className={styles.imagesViewsPhoto} src={imageView2} alt="View of product" />
                <img className={styles.imagesViewsPhoto} src={imageView3} alt="View of product" />
                <img className={styles.imagesViewsPhoto} src={imageView4} alt="View of product" />
                <img className={styles.imagesViewsPhoto} src={imageView5} alt="View of product" />
            </section>
        </div>
    );
}

export default ProductViews;






