import "./PageNotFound.css";


import { Link } from 'react-router-dom';

import { useState } from "react";

import Header from "../Header/Header";

import Footer from "../Footer/Footer";

let footeritems = {
  tel: {
    text: "+7 900 000 0000",
    link: "tel:79000000000",
  },
  mail: {
    text: "partner@mymarket.com",
    link: "mailto:partner@mymarket.com",
  },
  top: {
    text: "Наверх",
    link: "#Top",
  },
};

function PageNotFound() {
  const [ActiveButton, setActiveButton] = useState(
    JSON.parse(localStorage.getItem("sidebarButtonChoise"))
  );
  localStorage.setItem("sidebarButtonChoise", JSON.stringify(ActiveButton));
  const handleClickButton = () => setActiveButton((prevCount) => 1 - prevCount);

  const [ActiveLike, setActiveLike] = useState(
    JSON.parse(localStorage.getItem("sidebarLikeChoise"))
  );
  localStorage.setItem("sidebarLikeChoise", JSON.stringify(ActiveLike));
  const handleClickLike = () => setActiveLike((prevCount) => 1 - prevCount);

  console.log(setActiveLike);

  return (
    <div>
      <Header ActiveButton={ActiveButton} ActiveLike={ActiveLike} />
      <div className="errorText">
     <p className="errorPage"> Здесь должно быть содержимое главной страницы.<br/> Но в рамках курса главная страница используется лишь<br/> в демонстрационных целях<br/> </p> 
      <Link className="errorId" to="/"><a> Перейти на страницу товара </a> </Link>

      </div>
      <Footer item={footeritems} />
    </div>
  );
}

export default PageNotFound;
