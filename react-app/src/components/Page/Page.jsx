// import { useState } from 'react';
import './Page.css';
import Header from '../Header/Header';
import Navigation from '../Navigation/Navigation';
import Main from '../Main/Main';
import Footer from '../Footer/Footer';


let navigationList = [
  {
    text: 'Электроника',
    link: '/PageNotFound',
  },
  {
    text: 'Смартфоны и гаджеты',
    link: '/PageNotFound',
  },
  {
    text: 'Мобильные телефоны',
    link: '/PageNotFound'
  },
  {
    text: 'Apple',
    link: '/PageNotFound'
  },
];

let footeritems = {
  tel: {
    text: '+7 900 000 0000',
    link: 'tel:79000000000',
  },
  mail: {
    text: 'partner@mymarket.com',
    link: 'mailto:partner@mymarket.com',
  },
  top: {
    text: 'Наверх',
    link: '#Top',
  },
};

function Page() {

  // const [ActiveButton, setActiveButton] = useState(JSON.parse(localStorage.getItem('sidebarButtonChoise')));
  // localStorage.setItem('sidebarButtonChoise', JSON.stringify(ActiveButton));
  // const handleClickButton = () => setActiveButton((prevCount) => 1 - prevCount);

  // const [ActiveLike, setActiveLike] = useState(JSON.parse(localStorage.getItem('sidebarLikeChoise')));
  // localStorage.setItem('sidebarLikeChoise', JSON.stringify(ActiveLike));
  // const handleClickLike = () => setActiveLike((prevCount) => 1 - prevCount);

  // console.log(setActiveLike);
 
  return (
    <div>
      <Header
        // ActiveButton={ActiveButton}
        // ActiveLike={ActiveLike}
      />
      <div className="container-margin-auto">
        <div className="container-margin">
          <Navigation list={navigationList} />
          <Main
            // handleClickButton={handleClickButton}
            // ActiveButton={ActiveButton}
            // handleClickLike={handleClickLike}
            // ActiveLike={ActiveLike}
          />
        </div>
      </div>
      <Footer item={footeritems} />
    </div>
  );
}

export default Page;
