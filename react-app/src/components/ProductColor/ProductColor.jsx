import { useState } from 'react';
import './ProductColor.css';
import ColorButton from './ColorButton';



function ProductColor(props) {

    const { buttons } = props
    const [idActive, setIdActive] = useState(JSON.parse(localStorage.getItem('colorChoice')));
    const handleClick = (id) => setIdActive(id);
    localStorage.setItem('colorChoice', JSON.stringify(idActive));
    const buttonsFilter = buttons.filter((value) => value.color === idActive);
    const button = buttonsFilter[0];

    return (
        <div>
            <section className="product-color">
                <div className="product-color__name">
                    <h3 className="h3">Цвет товара: 
                    {' '}
                        {button?.name}
</h3>
                </div>

                <div className="product-color__images">
                    {
                        buttons.map((item) => {
                        
                            return (

                            <div onClick={() => handleClick(item.id)} key={item.id}>
                                
                                <ColorButton
                                    item={item}
                                    isActive={item.id === idActive}
                                />
                            </div>
                            )
                        })
                    }

                </div>
            </section>
        </div>
    );
}


export default ProductColor;






