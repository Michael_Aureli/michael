import { useState } from 'react';
import './Form.css';

const mistakeInitial = { name: null, grade: null, reviewText: null };


function Form(props) {

    const [input, setInput] = useState(JSON.parse(localStorage.getItem('reviewFormInput')));
    const [mistake, setMistake] = useState({ ...mistakeInitial });
    localStorage.setItem('reviewFormInput', JSON.stringify(input));

    const handleChangeName = (event) => { setInput({ ...input, name: event.target.value }) };
    const handleFocusName = (event) => { setMistake({ ...mistakeInitial }) };
    const handleChangeGrade = (event) => { setInput({ ...input, grade: (event.target.value) }) };
    const handleFocusGrade = (event) => { setMistake({ ...mistakeInitial }) };
    const handleChangeReviewText = (event) => { setInput({ ...input, reviewText: event.target.value }) };
    const handleFocusReviewText = (event) => { setMistake({ ...mistakeInitial }) };

    const handleSubmit = (event) => {
        event.preventDefault();

        const newMistake = {};

        if (input?.name.trim().length < 3) {
            newMistake.name = 'Имя не может быть короче двух символов';
            // console.log('ok');
            // console.log(input.name);
        } 
        if (input?.name.trim() === '') {

            newMistake.name = 'Вы забыли ввести Имя и Фамилию';
            // return;
        } 
       

        for (let i = 0; i < input?.name.length; i++) {

            let codeOfSymbol = input?.name.trim().charCodeAt(i);

            if (codeOfSymbol === 32) {

            } else if (codeOfSymbol > 64 && codeOfSymbol < 90) {

            } else if (codeOfSymbol > 96 && codeOfSymbol < 122) {

            } else if (codeOfSymbol > 1039 && codeOfSymbol < 1103) {

            } else {

                newMistake.name = 'Используйте буквы русского или английского алфавита';
            }
        }

        if (+input?.grade.trim() < 1 || +input?.grade.trim() > 5) {
            
            newMistake.grade = 'Оценка должна быть от 1 до 5';

        } 

        if (input?.reviewText.trim() === '') {

            newMistake.reviewText = 'Вы забыли ввести текст отзыва';
       
        }

        if (newMistake.name && newMistake.grade) {
            newMistake.grade = '';
        }

        if (newMistake.name && newMistake.grade && newMistake.reviewText) {
            newMistake.grade = '';
            newMistake.reviewText = '';
        }

        if (newMistake.grade && newMistake.reviewText) {
            newMistake.reviewText = '';
        }

        if (newMistake.name && newMistake.reviewText) {
            newMistake.reviewText = '';
        }

        if (input.name && input.grade && input.reviewText) {
            input.name = '';
            input.grade = '';
            input.reviewText = '';
        }

        setMistake(newMistake);

    };


        return (
            <div>
                <form className="form"
                    onSubmit={handleSubmit}
                >
                    <div className="form__name">
                        Добавить свой отзыв
                    </div>
                    <div className="form__content">
                        <div className="form__input">
                            <div className="input-container1">
                                <input
                                    className="input1"
                                    type="text"
                                    name="name"
                                    placeholder="Имя и фамилия"
                                    value={handleSubmit? input?.name :''}
                                    onChange={handleChangeName}
                                    onFocus={handleFocusName}
                                />
                                <div className={`input-mistake-name ${mistake?.name ? 'color' : ''}`}>{mistake?.name}</div>
                            </div>
                            <div className="input-container2"></div>
                            <div className="input-container3">
                                <input
                                    className="input2"
                                    type="text"
                                    name="grade"
                                    placeholder="Оценка"
                                    value={handleSubmit? input?.grade.trim() :''}
                                    onChange={handleChangeGrade}
                                    onFocus={handleFocusGrade}
                                />
                                <div className={`input-mistake-grade ${mistake?.grade ? 'color' : ''}`}>{mistake?.grade}</div>
                            </div>
                        </div>

                        <textarea
                            className="textarea"
                            name="text"
                            placeholder="Текст отзыва"
                            value={handleSubmit? input?.reviewText :''}
                            onChange={handleChangeReviewText}
                            onFocus={handleFocusReviewText}
                        ></textarea>
                        <div className={`input-mistake-review ${mistake?.reviewText ? 'color' : ''}`}>{mistake?.reviewText}</div>
                        <div className="form__button">
                            <button type="submit" className="form__button_button">Отправить отзыв</button>
                        </div>
                    </div>
                </form>

            </div>
        );
}

export default Form;
