import React from "react";
// import ReactDOM from "react-dom/client";
import "./index.css";
// import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Page from "./components/Page/Page";

import { BrowserRouter, Routes, Route } from "react-router-dom";
import PageIndex from "./components/PageIndex/PageIndex.jsx";

import PageNotFound from "./components/PageNotFound/PageNotFound.jsx";


import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import store, { persistor } from "./store";
import { PersistGate } from "redux-persist/integration/react";

// const root = ReactDOM.createRoot(document.getElementById("root"));
const rootElement = document.getElementById("root");
const root = createRoot(rootElement);

root.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Routes>
            <Route path="/" element={<Page />} />
            <Route path="/PageIndex" element={<PageIndex />} />
            <Route path="/*" element={<PageNotFound />} />
          </Routes>
        </PersistGate>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);

reportWebVitals();
